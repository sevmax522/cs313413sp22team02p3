package edu.luc.etl.cs313.android.shapes.model;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

    // TODO (DONE) entirely your job (except onCircle)

    @Override
    public Location onCircle(final Circle c) {
        final int radius = c.getRadius();
        return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
    }

    @Override
    public Location onFill(final Fill f) {
        Shape tempS = f.getShape();
        return tempS.accept(this);
    }

    @Override
    public Location onGroup(final Group g) {
        List<? extends Shape> shapes = g.getShapes();
        //Initializing our variables
        int minX = 0, minY = 0, maxX = 0, maxY = 0;
        int count = 0;
        //Declaring our objects
        Location loc;
        Rectangle rec;
        for(int i = 0; i < shapes.size(); i++) {
            //Defining our objects that we are looking at
            loc = shapes.get(i).accept(this);
            rec = (Rectangle)loc.shape;
            //The count keeps us on the right track and allows us to escape the
            // if statements when needed.
            if(count == 0) {
                minX = loc.getX();
                minY = loc.getY();
                maxX = loc.getX() + rec.getWidth();
                maxY = loc.getY() + rec.getHeight();
                //Testing for the right points to use in return statement
            } else {
                if(loc.getX() < minX) {
                    minX = loc.getX();
                }
                if(loc.getY() < minY) {
                    minY = loc.getY();
                }
                if(loc.getX() + rec.getWidth() > maxX) {
                    maxX = loc.getX() + rec.getWidth()
                }
                if(loc.getY() + rec.getHeight() > maxY) {
                    maxX = loc.getY() + rec.getHeight()
                }
            }
            count++;
        }
        return new Location(minX, minY, new Rectangle(maxX-minX, maxY-minY));
    }

    @Override
    public Location onLocation(final Location l) {
        //Declaring shape so it is easier manipulated
        Shape shapel = l.getShape();
        Location loc = shapel.accept(this);
        return new Location(loc.getX(), loc.getY(), loc.getShape());
    }

    @Override
    public Location onRectangle(final Rectangle r) {
        return new Location(0, 0, new Rectangle(r.getWidth(), r.getHeight()));
    }

    @Override
    public Location onStrokeColor(final StrokeColor c) {
        //Using recursion
        return c.getShape().accept(this);
    }

    @Override
    public Location onOutline(final Outline o) {
        //Using recursion
        return o.getShape().accept(this);
    }

    @Override
    public Location onPolygon(final Polygon s) {
        List<? extends Shape> shapes = g.getShapes();
        //Initializing our variables
        int minX = 0, minY = 0, maxX = 0, maxY = 0;
        int count = 0;
        //Declaring our object
        Point point;
        for(int i = 0; i < shapes.size(); i++) {
            point = (Point)shape;
            //The count keeps us on the right track and allows us to escape the
            // if statements when needed.
            if(count == 0) {
                minX = point.getX();
                minY = point.getY();
                maxX = point.getX();
                maxY = point.getY();
                //Testing for the right points to use in return statement
            } else {
                if(point.getX() < minX) {
                    minX = point.getX();
                }
                if(point.getY() < minY) {
                    minY = point.getY();
                }
                if(point.getX() > maxX) {
                    maxX = point.getX();
                }
                if(point.getY() > maxY) {
                    maxY = point.getY();
                }
            }
            count++;
        }
        return new Location(minX, minY, new Rectangle(maxX-minX, maxY-minY));
    }
}
