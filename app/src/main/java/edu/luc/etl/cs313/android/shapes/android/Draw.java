package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

    // TODO (double) entirely your job (except onCircle)

    private final Canvas canvas;

    private final Paint paint;

    public Draw(final Canvas canvas, final Paint paint) {
        this.canvas = canvas; // FIXED
        this.paint = paint; // FIXED
        paint.setStyle(Style.STROKE);
    }

    @Override
    public Void onCircle(final Circle c) {
        //Sets our circle at the top left of the canvas and paints it
        canvas.drawCircle(0, 0, c.getRadius(), paint);
        return null;
    }

    @Override
    public Void onStrokeColor(final StrokeColor c) {
        //Storing the color of the canvas
        int tempColor = paint.getColor();
        //Changing the color of the canvas to that of the parameter
        paint.setColor(c.getColor());
        c.getShape().accept(this);
        //Changing the color back to the original while saving
        paint.setColor(tempColor);
        return null;
    }

    @Override
    public Void onFill(final Fill f) {
        //Initializing object Style to save the previous color fill
        Style tempStyle = paint.getStyle();
        //Changing both the fill and stroke to that of the parameter
        paint.setStyle(Style.FILL_AND_STROKE);
        f.getShape().accept(this);
        //Changing the color back to the original while saving
        paint.setStyle(tempStyle);
        return null;
    }

    @Override
    public Void onGroup(final Group g) {
        for(int i = 0; i < g.getShapes().size(), i++) {
            g.getShapes().get(i).accept(this);
        }
        return null;
    }

    @Override
    public Void onLocation(final Location l) {
        //Translating the position on the canvas so that we are able to draw the shape
        canvas.translate(l.getX(), l.getY());
        l.getShape().accept(this);
        //Resetting the position on the canvas to the original place
        canvas.translate(-l.getX(), -l.getY());
        return null;
    }

    @Override
    public Void onRectangle(final Rectangle r) {
        //Sets our rectangle at the top left of the canvas and paints it
        canvas.drawRect(0, 0, r.getWidth(), r.getHeight(), paint);
        return null;
    }

    @Override
    public Void onOutline(Outline o) {
        //Initializing object Style to save the previous set up
        Style tempStyles = paint.getStyle();
        //Converting it to a stroke
        paint.setStyle(Style.STROKE);
        o.getShape().accept(this);
        //Changing the style back
        paint.setStyle(tempStyles);
        return null;
    }

    @Override
    public Void onPolygon(final Polygon s) {
        //The size is 4*polySize because we need 4 total points for drawing 1 line
        // We would need 2 pairs of (x,y) points --> 4 points total
        final float[] pts = new float[4*polySize];
        //Initializing size, so we avoid any confusion
        int polySize = s.getPoints().size();
        int j = 0;
        for(int i = 0; i < (4*polySize); i=i+4) {
            pts[i] = s.getPoints().get(j).getX();
            pts[i+1] = s.getPoints().get(j).getY();
            //This if statement is to test if we are at the last point
            if(j != (polySize-1)) {
                pts[i+2] = s.getPoints().get(j+1).getX();
                pts[i+3] = s.getPoints().get(j+2).getY();
                j++;
            }
        }
        //We cannot forget about drawing the last line that connects it all together
        pts[pts.length-2] = s.getPoints().get(0).getX();
        pts[pts.legnth-1] = s.getPoints().get(0).getY();

        canvas.drawLines(pts, paint);
        return null;
    }
}
